# params.pp is only to store default values away from the puppet code.
class mailserver::params {
  # Common parameters:
  $domain_name                     = undef
  $db_type                         = 'mysql'
  $db_name                         = 'servermail'
  $db_username                     = 'usermail'
  $db_password                     = 'mailpassword'
  $replicate_to                    = undef

  # Postfix parameters:
  $smtpd_sasl_auth                 = true
  $smtpd_tls_key_file              = '/etc/ssl/private/ssl-cert-snakeoil.key'
  $smtpd_tls_cert_file             = '/etc/ssl/certs/ssl-cert-snakeoil.pem'
  $inet_interfaces                 = $facts[networking][ip]
  $postfix_ssl                     = true
  $postfix_virtual_transport       = 'dovecot'
  $postfix_virtual_mailbox_domains = ['mysql:/etc/postfix/mysql-virtual-mailbox-domains.cf',]
  $postfix_virtual_mailbox_maps    = ['mysql:/etc/postfix/mysql-virtual-mailbox-maps.cf',]
  $postfix_virtual_alias_maps      = ['mysql:/etc/postfix/mysql-virtual-alias-maps.cf',]
  $spamassassin                    = true

  # Dovecot parameters:
  $dovecot_cert_location           = '/etc/dovecot/dovecot.pem'
  $dovecot_key_location            = '/etc/dovecot/private/dovecot.pem'
  $dovecot_replication_port        = '12345'
  $dovecot_doveadm_password        = 'password'

  # MySQL parameters:
  $mysql_serverid                  = undef
  $db_root_password                = 'password'
  $mysql_table_setup_location      = '/etc/mysql/mysql-table-setup.sql'
}
