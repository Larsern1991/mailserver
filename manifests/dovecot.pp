# Dovecot.pp
class mailserver::dovecot (
  $db_type                  = $::mailserver::db_type,
  $db_name                  = $::mailserver::db_name,
  $db_username              = $::mailserver::db_username,
  $db_password              = $::mailserver::db_password,
  $replicate_to             = $::mailserver::replicate_to,
  $cert_location            = $::mailserver::dovecot_cert_location,
  $key_location             = $::mailserver::dovecot_key_location,
  $dovecot_replication_port = $::mailserver::dovecot_replication_port,
  $dovecot_doveadm_password = $::mailserver::dovecot_doveadm_password,
) {
  package { 'dovecot-core':
    ensure => latest,
  }

  package { 'dovecot-imapd':
    ensure => latest,
  }

  package { 'dovecot-lmtpd':
    ensure => latest,
  }

  package { 'dovecot-mysql':
    ensure => latest,
  }

  $dovecot_sql_conf_ext_hash = {
    'db_type'     => $db_type,
    'db_name'     => $db_name,
    'db_username' => $db_username,
    'db_password' => $db_password,
  }
  file { '/etc/dovecot/dovecot-sql.conf.ext':
    content => epp('mailserver/dovecot-sql.conf.ext.epp', $dovecot_sql_conf_ext_hash),
    notify  => Service['dovecot'],
  }

  $dovecot_config_hash = {
    'replicate_to'     => $replicate_to,
    'replication_port' => $dovecot_replication_port,
    'doveadm_password' => $dovecot_doveadm_password,
  }
  file { '/etc/dovecot/dovecot.conf':
    content => epp('mailserver/dovecot.conf.epp', $dovecot_config_hash),
    notify  => Service['dovecot'],
  }

  file { '/etc/dovecot/conf.d/10-auth.conf':
    source => 'puppet:///modules/mailserver/10-auth.conf',
    notify => Service['dovecot'],
  }

  file { '/etc/dovecot/conf.d/10-mail.conf':
    source =>'puppet:///modules/mailserver/10-mail.conf',
    notify => Service['dovecot'],
  }

  file { '/etc/dovecot/conf.d/10-master.conf':
    source => 'puppet:///modules/mailserver/10-master.conf',
    notify => Service['dovecot'],
  }

  $ssl_conf_hash = {
    'cert_location' => $cert_location,
    'key_location'  => $key_location,
  }
  file { '/etc/dovecot/conf.d/10-ssl.conf':
    content => epp('mailserver/10-ssl.conf.epp', $ssl_conf_hash),
    notify  => Service['dovecot'],
  }

  file { '/etc/dovecot/conf.d/auth-sql.conf.ext':
    source =>'puppet:///modules/mailserver/auth-sql.conf.ext',
    notify => Service['dovecot'],
  }

  file { '/etc/dovecot/dovecot.pem':
    source => 'puppet:///modules/mailserver/dovecot.pem',
    notify => Service['dovecot'],
  }

  file { '/etc/dovecot/private/dovecot.pem':
    source => 'puppet:///modules/mailserver/privatedovecot.pem',
    notify => Service['dovecot'],
  }

  service { 'dovecot':
    ensure => running,
  }

  # This user is needed to manage the postboxes on the server.
  accounts::user { 'vmail':
    uid       => '5000',
    gid       => '5000',
    groups    => ['vmail',],
    shell     => '/bin/bash',
    locked    => true,
    home      => '/var/mail',
    home_mode => '0770',
    notify    => Service['dovecot'],
  }

}
