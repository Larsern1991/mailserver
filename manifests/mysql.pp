# Mysql.pp
class mailserver::mysql (
  $domain_name                = $::mailserver::domain_name,
  $db_name                    = $::mailserver::db_name,
  $db_username                = $::mailserver::db_username,
  $db_password                = $::mailserver::db_password,
  $serverid                   = $::mailserver::mysql_serverid,
  $db_root_password           = $::mailserver::db_root_password,
  $mysql_table_setup_location = $::mailserver::mysql_table_setup_location,
  $replicate_to               = $::mailserver::replicate_to,
) {
  $override_options  = {
    'mysqld' => {
      'bind-address'     => '0.0.0.0',
      'server-id'        => $serverid,
      'binlog-format'    => 'mixed',
      'binlog-ignore-db' => 'mysql',
      'datadir'          => '/var/lib/mysql',
      'log-bin'          => '/var/log/mysql/mysql-bin.log',
    },
  }
  class { '::mysql::server':
    root_password           => $db_root_password,
    remove_default_accounts => true,
    override_options        => $override_options,
  }

  $mysql_table_setup_hash = {
    'domain'       => $domain_name,
    'db_name'      => $db_name,
    'replicate_to' => $replicate_to,
  }
  file { $mysql_table_setup_location:
    content => epp('mailserver/mysql-table-setup.sql.epp', $mysql_table_setup_hash),
    notify  => Service['mysql'],
  }

  mysql::db { $db_name:
    user        => $db_username,
    password    => $db_password,
    host        => 'localhost',
    grant       => ['SELECT', 'UPDATE', 'INSERT'],
    sql         => $mysql_table_setup_location,
    enforce_sql => true,
  }

  mysql_user { 'replication@%':
    ensure        => 'present',
    password_hash => mysql_password('password'),
  }

  mysql_grant {'replication@%/*.*':
    ensure     => 'present',
    privileges => ['REPLICATION SLAVE'],
    table      => '*.*',
    user       => 'replication@%',
  }
}
