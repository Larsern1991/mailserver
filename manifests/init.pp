# main class
class mailserver (
  # Common parameters:
  $domain_name                     = $::mailserver::params::domain_name,
  $db_type                         = $::mailserver::params::db_type,
  $db_name                         = $::mailserver::params::db_name,
  $db_username                     = $::mailserver::params::db_username,
  $db_password                     = $::mailserver::params::db_password,
  $replicate_to                    = $::mailserver::params::replicate_to,

  #Postfix parameters:
  $smtpd_sasl_auth                 = $::mailserver::params::smtpd_sasl_auth,
  $smtpd_tls_key_file              = $::mailserver::params::smtpd_tls_key_file,
  $smtpd_tls_cert_file             = $::mailserver::params::smtpd_tls_cert_file,
  $inet_interfaces                 = $::mailserver::params::inet_interfaces,
  $postfix_ssl                     = $::mailserver::params::postfix_ssl,
  $postfix_virtual_transport       = $::mailserver::params::postfix_virtual_transport,
  $postfix_virtual_mailbox_domains = $::mailserver::params::postfix_virtual_mailbox_domains,
  $postfix_virtual_mailbox_maps    = $::mailserver::params::postfix_virtual_mailbox_maps,
  $postfix_virtual_alias_maps      = $::mailserver::params::postfix_virtual_alias_maps,
  $spamassassin                    = $::mailserver::params::spamassassin,

  #Dovecot parameters:
  $dovecot_cert_location           = $::mailserver::params::dovecot_cert_location,
  $dovecot_key_location            = $::mailserver::params::dovecot_key_location,
  $dovecot_replication_port        = $::mailserver::params::dovecot_replication_port,
  $dovecot_doveadm_password        = $::mailserver::params::dovecot_doveadm_password,

  #MySQL parameters:
  $mysql_serverid                  = $::mailserver::params::mysql_serverid,
  $db_root_password                = $::mailserver::params::db_root_password,
  $mysql_table_setup_location      = $::mailserver::params::mysql_table_setup_location,
) inherits mailserver::params {

  contain mailserver::postfix
  contain mailserver::mysql
  contain mailserver::dovecot
}

