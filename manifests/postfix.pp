# postfix.pp
class mailserver::postfix (
  # Main parameters needed for setting up mysql. Only supported use is SMTPD.
  $domain_name                     = $::mailserver::domain_name,
  $smtpd_sasl_auth                 = $::mailserver::smtpd_sasl_auth,
  $smtpd_tls_key_file              = $::mailserver::smtpd_tls_key_file,
  $smtpd_tls_cert_file             = $::mailserver::smtpd_tls_cert_file,
  $inet_interfaces                 = $::mailserver::inet_interfaces,
  $postfix_ssl                     = $::mailserver::postfix_ssl,
  $spamassassin                    = $::mailserver::spamassassin,

  # Which mailbox handler is used. Only supported and default is 'dovecot'.
  $postfix_virtual_transport       = $::mailserver::postfix_virtual_transport,

  # What type of database and database mapping with queries and users to acces the database. Only supported and default is 'mysql'.
  $db_type                         = $::mailserver::db_type,
  $db_name                         = $::mailserver::db_name,
  $db_username                     = $::mailserver::db_username,
  $db_password                     = $::mailserver::db_password,
  $postfix_virtual_mailbox_domains = $::mailserver::postfix_virtual_mailbox_domains,
  $postfix_virtual_mailbox_maps    = $::mailserver::postfix_virtual_mailbox_maps,
  $postfix_virtual_alias_maps      = $::mailserver::postfix_virtual_alias_maps,
) {
  class { '::postfix::server':
    mydomain                => $domain_name,
    virtual_transport       => $postfix_virtual_transport,
    virtual_mailbox_domains => $postfix_virtual_mailbox_domains,
    virtual_mailbox_maps    => $postfix_virtual_mailbox_maps,
    virtual_alias_maps      => $postfix_virtual_alias_maps,
    spamassassin            => $spamassassin,
    smtpd_sasl_auth         => $smtpd_sasl_auth,
    smtpd_tls_key_file      => $smtpd_tls_key_file,
    smtpd_tls_cert_file     => $smtpd_tls_cert_file,
    inet_interfaces         => $inet_interfaces,
    ssl                     => $postfix_ssl,
  }

  package { 'postfix-mysql':
    ensure => latest,
  }

  # These files tell postfix how to access the database to get domains, accounts and aliases used in this mailserver.
  $virtual_db_user_password_hash = {
    'db_name'     => $db_name,
    'db_username' => $db_username,
    'db_password' => $db_password,
  }
  file { '/etc/postfix/mysql-virtual-mailbox-domains.cf':
    content => epp('mailserver/mysql-virtual-mailbox-domains.cf.epp', $virtual_db_user_password_hash),
    notify  => Service['postfix'],
  }
  file { '/etc/postfix/mysql-virtual-mailbox-maps.cf':
    content => epp('mailserver/mysql-virtual-mailbox-maps.cf.epp', $virtual_db_user_password_hash),
    notify  => Service['postfix'],
  }
  file { '/etc/postfix/mysql-virtual-alias-maps.cf':
    content => epp('mailserver/mysql-virtual-alias-maps.cf.epp', $virtual_db_user_password_hash),
    notify  => Service['postfix'],
  }
}
