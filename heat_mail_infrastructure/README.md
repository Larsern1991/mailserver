# Mail-infrastructure-heat
Heat templates for the mail deployment project in NTNU course [Infrastructure as Code (IaC)](https://www.ntnu.edu/studies/courses/IMT3005).

Your keypair must be provided in `env_prod_mail.yaml` and `env_dev_mail.yaml`, and two security groups (in addition to the default group): `linux`, `dns`, `mail` should exist.

`base_mail.yaml` creates the servers `manager`, `monitor` and `dns`, and a network: `mail_net`. 
`main_mail.yaml` (which depends on `base_mail.yaml`) creates the servers that are supposed to run the mail service. Create stack with e.g.

```bash
Production environment:
openstack stack create prod -t prod_mail.yaml -e env_prod_mail.yaml

Testing environment:
openstack stack create test -t test_mail.yaml -e env_test_mail.yaml
```
