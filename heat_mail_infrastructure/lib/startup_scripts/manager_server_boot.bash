#!/bin/bash

tempdeb=$(mktemp /tmp/debpackage.XXXXXXXXXXXXXXXXXX) || exit 1
wget -O "$tempdeb" https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
dpkg -i "$tempdeb"
apt-get update
apt-get -y install puppetserver
export PATH=$PATH:/opt/puppetlabs/bin/

echo "$(ip a | grep -Eo 'inet ([0-9]*\.){3}[0-9]*' | tr -d 'inet ' | grep -v '^127') $(hostname).environmentsuffix.imt3005mail.com $(hostname)" >> /etc/hosts

/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true

cat <<EOF >> /etc/dhcp/dhclient.conf
supersede domain-name "environmentsuffix.imt3005mail.com";
supersede domain-name-servers dns_ip_address;
EOF


#install r10k
puppet module install puppet-r10k
#configure pull from git-repo
echo "
class { 'r10k':
  sources => {
    'puppet' => {
      'remote'  => 'https://petthaa2@bitbucket.org/petthaa2/control-repo.git',
      'basedir' => '/etc/puppetlabs/code/environments',
      'prefix'  => false,
    }, 
  },
}" > /var/tmp/r10k.pp

#Spin up r10k
puppet apply /var/tmp/r10k.pp

reboot
