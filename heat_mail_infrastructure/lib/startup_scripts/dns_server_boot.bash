#!/bin/bash

echo "$(ip a | grep -Eo 'inet ([0-9]*\.){3}[0-9]*' | tr -d 'inet ' | grep -v '^127') $(hostname).environment.imt3005mail.com $(hostname)" >> /etc/hosts
cat <<EOF >> /etc/dhcp/dhclient.conf
supersede domain-name "environment.imt3005mail.com";
supersede domain-name-servers $(ip a | grep -Eo 'inet ([0-9]*\.){3}[0-9]*' | tr -d 'inet ' | grep -v '^127');
EOF
reboot
