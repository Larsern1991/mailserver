#!/bin/bash

tempdeb=$(mktemp /tmp/debpackage.XXXXXXXXXXXXXXXXXX) || exit 1
wget -O "$tempdeb" https://apt.puppetlabs.com/puppetlabs-release-pc1-xenial.deb
dpkg -i "$tempdeb"
apt-get update
apt-get -y install puppet-agent
echo "$(ip a | grep -Eo 'inet ([0-9]*\.){3}[0-9]*' | tr -d 'inet ' | grep -v '^127') $(hostname).environment.imt3005mail.com $(hostname)" >> /etc/hosts
echo "manager_ip_address manager.environment.imt3005mail.com manager" >> /etc/hosts
cat <<EOF >> /etc/puppetlabs/puppet/puppet.conf
[main]
    server = manager.environment.imt3005mail.com
EOF
/opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
cat <<EOF >> /etc/dhcp/dhclient.conf
supersede domain-name "environment.imt3005mail.com";
supersede domain-name-servers dns_ip_address;
EOF
reboot
