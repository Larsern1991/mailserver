require 'spec_helper'

describe 'mailserver::mysql' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:facts) { {
    osfamily: 'Debian',
    operatingsystem: 'Ubuntu',
    operatingsystemrelease: '14.04',
    lsbdistcodename: 'trusty',
    operatingsystemmajrelease: '14.04',
    root_home: '/root',
    puppetversion: '4.10.8',
  } }


  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) { {
    domain_name: 'dev.imt3005mail.com',
    db_name: 'servermail',
    db_username: 'usermail',
    db_password: 'mailpassword',
    serverid: '1',
    db_root_password: 'password',
    mysql_table_setup_location: '/etc/mysql/mysql-table-setup.sql',
    replicate_to: 'mail1.dev.imt3005mail.com',
  } }

  it do
    is_expected.to contain_class("mysql::server")
        .with({
          "remove_default_accounts" => true,
          })
  end
    
  it do
    is_expected.to contain_file("/etc/mysql/mysql-table-setup.sql")
        .with({
#          "content" => [],
          "notify" => "Service[mysql]",
          })
  end
    
  it do
    is_expected.to contain_mysql__db("servermail")
        .with({
#          "user" => [],
#          "password" => [],
          "host" => "localhost",
          "grant" => ["SELECT", "UPDATE", "INSERT"],
          "sql" => "/etc/mysql/mysql-table-setup.sql",
          "enforce_sql" => true,
          })
  end
    
  it do
    is_expected.to contain_mysql_user("replication@%")
        .with({
          "ensure" => "present",
#          "password_hash" => [],
          })
  end
    
  it do
    is_expected.to contain_mysql_grant("replication@%/*.*")
        .with({
          "ensure" => "present",
          "privileges" => ["REPLICATION SLAVE"],
          "table" => "*.*",
          "user" => "replication@%",
          })
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)
  
end
