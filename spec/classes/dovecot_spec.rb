require 'spec_helper'

describe 'mailserver::dovecot' do
  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:facts) { {
    operatingsystem: 'Ubuntu',
  } }

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) { {
    db_type: 'mysql',
    db_name: 'servermail',
    db_username: 'usermail',
    db_password: 'mailpassword',
    replicate_to: 'mail1.dev.imt3005mail.com',
    cert_location: '/etc/dovecot/dovecot.pem',
    key_location: '/etc/dovecot/private/dovecot.pem',
    dovecot_replication_port: '12345',
    dovecot_doveadm_password: 'password',
  } }

  it do
    is_expected.to contain_package("dovecot-core")
        .with({
          "ensure" => "latest",
          })
  end
    
  it do
    is_expected.to contain_package("dovecot-imapd")
        .with({
          "ensure" => "latest",
          })
  end
    
  it do
    is_expected.to contain_package("dovecot-lmtpd")
        .with({
          "ensure" => "latest",
          })
  end
    
  it do
    is_expected.to contain_package("dovecot-mysql")
        .with({
          "ensure" => "latest",
          })
  end
    
  it do
    is_expected.to contain_file("/etc/dovecot/dovecot-sql.conf.ext")
        .with({
#          "content" => [],
          "notify" => "Service[dovecot]",
          })
  end
    
  it do
    is_expected.to contain_file("/etc/dovecot/dovecot.conf")
        .with({
#          "content" => [],
          "notify" => "Service[dovecot]",
          })
  end
    
  it do
    is_expected.to contain_file("/etc/dovecot/conf.d/10-auth.conf")
        .with({
          "source" => "puppet:///modules/mailserver/10-auth.conf",
          "notify" => "Service[dovecot]",
          })
  end
    
  it do
    is_expected.to contain_file("/etc/dovecot/conf.d/10-mail.conf")
        .with({
          "source" => "puppet:///modules/mailserver/10-mail.conf",
          "notify" => "Service[dovecot]",
          })
  end
    
  it do
    is_expected.to contain_file("/etc/dovecot/conf.d/10-master.conf")
        .with({
          "source" => "puppet:///modules/mailserver/10-master.conf",
          "notify" => "Service[dovecot]",
          })
  end
    
  it do
    is_expected.to contain_file("/etc/dovecot/conf.d/10-ssl.conf")
        .with({
#          "content" => [],
          "notify" => "Service[dovecot]",
          })
  end
    
  it do
    is_expected.to contain_file("/etc/dovecot/conf.d/auth-sql.conf.ext")
        .with({
          "source" => "puppet:///modules/mailserver/auth-sql.conf.ext",
          "notify" => "Service[dovecot]",
          })
  end
    
  it do
    is_expected.to contain_file("/etc/dovecot/dovecot.pem")
        .with({
          "source" => "puppet:///modules/mailserver/dovecot.pem",
          "notify" => "Service[dovecot]",
          })
  end
    
  it do
    is_expected.to contain_file("/etc/dovecot/private/dovecot.pem")
        .with({
          "source" => "puppet:///modules/mailserver/privatedovecot.pem",
          "notify" => "Service[dovecot]",
          })
  end
    
  it do
    is_expected.to contain_service("dovecot")
        .with({
          "ensure" => "running",
          })
  end
    
  it do
    is_expected.to contain_accounts__user("vmail")
        .with({
          "uid" => "5000",
          "gid" => "5000",
          "groups" => ["vmail"],
          "shell" => "/bin/bash",
          "locked" => true,
          "home" => "/var/mail",
          "home_mode" => "0770",
          "notify" => "Service[dovecot]",
          })
  end
 
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

end
